# :sparkles: Triangle Tracker

__Version 1.1.0: August 11, 2016__
## by [Karen Freeman-Smith](http://karenfreemansmith.github.io) and [Aimen Khakwani](http://aimenkhakwani.github.io)

### Description
__*A webpage to analyze triangles. Give us 3 sides and we will tell you if it is a triangle and what kind of triangle.*__


### Setup/Installation
*None required. Clone or download and extract to use*

### Support & Contact
For questions or comments, please __email [Karen](karenfreemansmith@gmail.com)__ or __email [Aimen](aimen.khakwani@hotmail.com)__

### Known Issues
* no known issues

### Technologies Used
###### HTML, CSS, Bootstrap, JavaScript, jQuery

### Legal
*Licensed under the GNU General Public License v3.0*

Copyright (c) 2016 **_Karen Freeman-Smith_ & _Aimen Khakwani_**
